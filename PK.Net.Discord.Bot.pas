﻿(*
 * Simple Discord Talking Bot
 *
 * PLATFORMS
 *   Windows / macOS / iOS / Android / Linux
 *
 * ENVIRONMENT
 *   Delphi 10.3.x Rio, Maybe 10.x
 *
 * REQUIRED LIBRARY
 *   PK.Net.WebSocket
 *
 * LICENSE
 *   Copyright (C) 2019 HOSOKAWA Jun (Twitter: @pik)
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * HISTORY
 *   2019/05/08 Version 1.0.0
 *   2019/05/17 Version 1.0.1  CHANGE: Mention judgment method
 *                             FIX: Mention message bug
 *
 * EXAMPLE
 *   program HelloBot;
 *
 *   {$APPTYPE CONSOLE}
 *
 *   uses
 *     PK.Net.Discord.Bot;
 *
 *   var
 *     FBot: TBotApplication;
 *   begin
 *     FBot := TBotApplication.Create(TOKEN);
 *     try
 *       FBot.ErrorHandler :=
 *         procedure (const iErrCode: Integer; const iMessage: String)
 *         begin
 *           Writeln(Format('ERROR (%d): %s', [iErrCode, iMessage]));
 *         end;
 *
 *       FBot.ReadyHandler :=
 *         procedure
 *         begin
 *           Writeln('Connected.');
 *         end;
 *
 *       FBot.MentionHandler :=
 *         procedure (
 *           const iChannelID, iFromUserID, iFromUserName, iMessage: String)
 *         begin
 *           Writeln(
 *             Format(
 *               '#%s: %s(%s) say "%s"',
 *               [iChannelID, iFromUserName, iFromUserID, iMessage])
 *           );
 *           FBot.Send(iChannelID, 'Hello, Discord!');
 *         end;
 *
 *       FBot.Run;
 *     finally
 *       FBot.DisposeOf;
 *     end;
 *   end.
 *)

unit PK.Net.Discord.Bot;

interface

uses
  System.Classes
  , System.SysUtils
  , System.Types
  , System.UITypes
  , System.JSON
  , System.Net.HttpClient
  , PK.Net.WebSocket
  ;

type
  TDicordTalkBot = class
  public type
    TErrorEvent =
      procedure (
        Sender: TObject;
        const iErrCode: Integer;
        const iMessage: String) of object;
    TDispatchEvent =
      procedure (
        Sender: TObject;
        const iEventType: String;
        const iData: TJsonValue) of object;
    TMessageEvent = procedure (Sender: TObject; const iData: String) of object;
    TMentionEvent =
      procedure (
        Sender: TObject;
        const iChannelID, iFromUserID, iFromUserName, iMessage: String
      ) of object;
  public const
    ERR_CODE_SEND_FAILED = 1000;

    EVENT_CHANNEL_CREATE = 'CHANNEL_CREATE';
    EVENT_CHANNEL_DELETE = 'CHANNEL_DELETE';
    EVENT_CHANNEL_PINS_UPDATE = 'CHANNEL_PINS_UPDATE';
    EVENT_CHANNEL_UPDATE = 'CHANNEL_UPDATE';
    EVENT_GUILD_AVAILABLE = 'GUILD_AVAILABLE';
    EVENT_GUILD_BAN_ADD = 'GUILD_BAN_ADD';
    EVENT_GUILD_BAN_REMOVE = 'GUILD_BAN_REMOVE';
    EVENT_GUILD_CREATE = 'GUILD_CREATE';
    EVENT_GUILD_DELETE = 'GUILD_DELETE';
    EVENT_GUILD_EMOJIS_UPDATE = 'GUILD_EMOJIS_UPDATE';
    EVENT_GUILD_INTEGRATIONS_UPDATE = 'GUILD_INTEGRATIONS_UPDATE';
    EVENT_GUILD_MEMBER_ADD = 'GUILD_MEMBER_ADD';
    EVENT_GUILD_MEMBER_REMOVE = 'GUILD_MEMBER_REMOVE';
    EVENT_GUILD_MEMBER_UPDATE = 'GUILD_MEMBER_UPDATE';
    EVENT_GUILD_ROLE_CREATE = 'GUILD_ROLE_CREATE';
    EVENT_GUILD_ROLE_DELETE = 'GUILD_ROLE_DELETE';
    EVENT_GUILD_ROLE_UPDATE = 'GUILD_ROLE_UPDATE';
    EVENT_GUILD_UNAVAILABLE = 'GUILD_UNAVAILABLE';
    EVENT_GUILD_UPDATE = 'GUILD_UPDATE';
    EVENT_MESSAGE_CREATE = 'MESSAGE_CREATE';
    EVENT_MESSAGE_DELETE = 'MESSAGE_DELETE';
    EVENT_MESSAGE_DELETE_BULK = 'MESSAGE_DELETE_BULK';
    EVENT_MESSAGE_EMBEDS_UPDATE = 'MESSAGE_EMBEDS_UPDATE';
    EVENT_MESSAGE_REACTION_ADD = 'MESSAGE_REACTION_ADD';
    EVENT_MESSAGE_REACTION_REMOVE = 'MESSAGE_REACTION_REMOVE';
    EVENT_MESSAGE_REACTION_REMOVE_ALL = 'MESSAGE_REACTION_REMOVE_ALL';
    EVENT_MESSAGE_UPDATE = 'MESSAGE_UPDATE';
    EVENT_PRESENCE_UPDATE = 'PRESENCE_UPDATE';
    EVENT_READY = 'READY';
    EVENT_RESUMED = 'RESUMED';
    EVENT_TYPING_START = 'TYPING_START';
    EVENT_USER_UPDATE = 'USER_UPDATE';
    EVENT_VOICE_SERVER_UPDATE = 'VOICE_SERVER_UPDATE';
    EVENT_VOICE_STATE_UPDATE = 'VOICE_STATE_UPDATE';
    EVENT_WEBHOOKS_UPDATE = 'WEBHOOKS_UPDATE';
  private type
    TOpCode = (
      None = -1,
      Dispatch = 0,
      Heartbeat = 1,
      Identify = 2,
      StatusUpdate = 3,
      VoiceStateUpdate = 4,
      VoiceServerPing = 5,
      Resume = 6,
      Reconnect = 7,
      RequestGuildMembers = 8,
      InvalidSession = 9,
      Hello = 10,
      HeartbeatACK = 11
    );

    TChannelTypes = (
      GUILD_TEXT = 0,
      DM = 1,
      GUILD_VOICE = 2,
      GROUP_DM = 3,
      GUILD_CATEGORY = 4,
      GUILD_NEWS = 5,
      GUILD_STORE = 6
    );
  private const
    DEF_RECONNECTION_INTERVAL = 5 * 1000; // [msec]

    SELF_VERSION = 1;

    DISCORD_GATEWAY_URL = 'wss://gateway.discord.gg/?v=6&encoding=json';
    DISCORD_HTTP_URL = 'https://discordapp.com/api/v6/';
    DISCORD_IMAGE_URL = 'https://cdn.discordapp.com';

    CMD_CREATE_MESSAGE = 'channels/%s/messages';

    HEADER_USER_AGENT_VALUE = ' DiscordBot (PK.Net.Discord.Bot, %d)';
    HEADER_AUTHORIZATION = 'Authorization';
    HEADER_AUTHORIZATION_VALUE = 'Bot %s';
  private var
    FSocket: TWebSocket;
    FToken: String;
    FSeq: Integer;
    FSessionID: String;
    FResuming: Boolean;
    FUserName: String;
    FUserId: String;
    FMentionTag: String;
    FMentionTagLen: Integer;
    FHttpClient: THttpClient;
    FConnecting: Boolean;
    FClosing: Boolean;
    FOnMention: TMentionEvent;
    FOnError: TErrorEvent;
    FOnDispatch: TDispatchEvent;
    FOnReady: TNotifyEvent;
    FOnMessage: TMessageEvent;
  private
    procedure SocketError(
      Sender: TObject;
      const iErrCode: Integer;
      const iMessage: String);
    procedure SocketClosed(
      Sender: TObject;
      const iCloseCode: Integer;
      const iCloseData: TBytes);
    procedure SocketHeartbeat(Sender: TObject);
    procedure SocketText(Sender: TObject; const iText: String);

    function GetOSName: String;
    function JsonRecToStr<T>(const iRec: T): String;
    function SendPayload<T>(const iOpCode: TOpCode; const iD: T): String;
    procedure SendHeartbeat;
    procedure SendIdentify;
    procedure SendResume;

    function CreateMessageBody(const iMessage, iToUser: String): String;
    procedure CallOnError(const iErrCode: Integer; const iMessage: String);
  public
    constructor Create(const iToken: String); reintroduce;
    destructor Destroy; override;
    procedure Open;
    procedure Close;
    procedure Send(
      const iChannelID, iMessage: String;
      const iToUser: String = '');
    procedure SendFile(
      const iChannelID, iMessage: String;
      const iFile: TStream;
      const iFileName: String;
      const iToUser: String = '');
    procedure SendRaw(
      const iCMDURL, iPayloadJsonStr: String;
      const iStream: TStream;
      const iStreamName: String);
  public
    property Connecting: Boolean read FConnecting;
    property OSName: String read GetOSName;
    property UserName: String read FUserName;
    property UserId: String read FUserId;
  public
    property OnError: TErrorEvent read FOnError write FOnError;
    property OnReady: TNotifyEvent read FOnReady write FOnReady;
    property OnDispatch: TDispatchEvent read FOnDispatch write FOnDispatch;
    property OnMessage: TMessageEvent read FOnMessage write FOnMessage;
    property OnMention: TMentionEvent read FOnMention write FOnMention;
  end;

  TBotApplication = class
  public type
    TErrorHandler =
      reference to procedure (const iErrCode: Integer; const iMessage: String);
    TReadyHandler = reference to procedure;
    TDispatchHandler =
      reference to
      procedure (const iEventType: String; const iData: TJsonValue);
    TMessageHandler = reference to procedure (const iData: String);
    TMentionHandler =
      reference to
      procedure (
        const iChannelID, iFromUserID, iFromUserName, iMessage: String
      );
    TCommandHandler =
      reference to
      procedure (
        const
          iChannelID, iFromUserID, iFromUserName, iCommand, iParameter: String;
        var
          ioDone: Boolean
      );
  private const
    DEF_COMMAND_CHAR = '!';
    CONNECTING_TIMEOUT_INTERVAL = 15 * 1000; // [msec]
  private var
    FBot: TDicordTalkBot;
    FRunning: Boolean;
    FStartTime: TDateTime;
    FCommandChar: Char;
    FMentionHandler: TMentionHandler;
    FMessageHandler: TMessageHandler;
    FReadyHandler: TReadyHandler;
    FDispatchHandler: TDispatchHandler;
    FErrorHandler: TErrorHandler;
    FCommandHandler: TCommandHandler;
  private
    function GetOSName: String;
    function GetElapsedSeconds: UInt64;
    procedure BotError(
      Sender: TObject;
      const iErrCode: Integer;
      const iMessage: String);
    procedure BotReady(Sender: TObject);
    procedure BotDispatch(
      Sender: TObject;
      const iEventType: String;
      const iData: TJsonValue); virtual;
    procedure BotMessage(Sender: TObject; const iData: String);
    procedure BotMention(
      Sender: TObject;
      const iChannelID, iFromUserID, iFromUserName, iMessage: String);
  protected
    procedure DoError(const iErrCode: Integer; const iMessage: String); virtual;
    procedure DoReady; virtual;
    procedure DoDispatch(
      const iEventType: String;
      const iData: TJsonValue); virtual;
    procedure DoMessage(const iData: String); virtual;
    procedure DoMention(
      const iChannelID, iFromUserID, iFromUserName, iMessage: String);
  public
    constructor Create(const iToken: String); virtual;
    destructor Destroy; override;
    procedure Run;
    procedure Stop;
    procedure Send(
      const iChannelID, iMessage: String;
      const iToUser: String = '';
      const iFile: TStream = nil;
      const iFileName: String = '');
  public
    property Running: Boolean read FRunning;
    property StartTime: TDateTime read FStartTime;
    property ElapsedSeconds: UInt64 read GetElapsedSeconds;
    property OSName: String read GetOSName;
    property CommandChar: Char read FCommandChar write FCommandChar;
    property ErrorHandler: TErrorHandler read FErrorHandler write FErrorHandler;
    property ReadyHandler: TReadyHandler read FReadyHandler write FReadyHandler;
    property DispatchHandler: TDispatchHandler
      read FDispatchHandler write FDispatchHandler;
    property MessageHandler: TMessageHandler
      read FMessageHandler write FMessageHandler;
    property MentionHandler: TMentionHandler
      read FMentionHandler write FMentionHandler;
    property CommandHandler: TCommandHandler
      read FCommandHandler write FCommandHandler;
  end;

implementation

uses
  System.IOUtils
  , System.DateUtils
  , System.JSON.Serializers
  , System.JSON.Types
  , System.Net.Mime
  , System.Net.URLClient
  , REST.Types
  ;

type
  TProperties = record
    [JsonName('$os')]
    os: String;
    [JsonName('$browser')]
    browser: String;
    [JsonName('$device')]
    device: String;
  end;

  TPresence = record
    status: String;
    afk: Boolean;
  end;

  TIdentify = record
    token: String;
    properties: TProperties;
    compress: Boolean;
    large_threshold: Integer;
    shard: TArray<Integer>;
    presence: TPresence;
  end;

  TResume = record
    token: String;
    session_id: String;
    seq: Integer;
  end;

  TChannel = record
    id: String;
    [JsonName('type')]
    type_: Integer
  end;

  TImage = record
    url: String;
  end;

  TImageEmbed = class
  public
    image: TImage;
  end;

  TCreateMessage = record
    content: String;
    tts: Boolean;
    embed: TImageEmbed;
  end;

{ TDicordTalkBot }

procedure TDicordTalkBot.CallOnError(
  const iErrCode: Integer;
  const iMessage: String);
begin
  if Assigned(FOnError) then
    FOnError(Self, iErrCode, iMessage);
end;

procedure TDicordTalkBot.Close;
begin
  FSeq := -1;
  FClosing := True;
  FSocket.Close;
end;

constructor TDicordTalkBot.Create(const iToken: String);
begin
  inherited Create;

  FToken := iToken;

  FSeq := -1;
  FSessionID := '';

  // WebSocket
  FSocket := TWebSocket.Create(nil);
  FSocket.OnError := SocketError;
  FSocket.OnClosed := SocketClosed;
  FSocket.OnHeartBeat := SocketHeartbeat;
  FSocket.OnText := SocketText;

  // HTTP
  FHttpClient := THttpClient.Create;
end;

function TDicordTalkBot.CreateMessageBody(
  const iMessage, iToUser: String): String;
var
  Body: TCreateMessage;
begin
  // Body
  var User := '';
  if not iToUser.IsEmpty then
  begin
    User := iToUser;
    if not iToUser.StartsWith('@') then
      User := '@' + User;

    User := '<' + User + '> ';
  end;

  Body.content := User + iMessage;
  Body.tts := False;
  Body.embed := nil;

  Result := JsonRecToStr(Body);
end;

destructor TDicordTalkBot.Destroy;
begin
  Close;

  FHttpClient.DisposeOf;
  FSocket.DisposeOf;

  inherited;
end;

function TDicordTalkBot.GetOSName: String;
const
  OS_NAME: array [TOSVersion.TPlatform] of String = (
    'Windows', 'MacOS', 'iOS', 'Android', 'WinRT', 'Linux'
  );
begin
  Result := OS_NAME[TOSVersion.Platform];
end;

function TDicordTalkBot.JsonRecToStr<T>(const iRec: T): String;
begin
  var S := TJsonSerializer.Create;
  try
    Result := S.Serialize(iRec);
  finally
    S.DisposeOf;
  end;
end;

procedure TDicordTalkBot.Open;
begin
  FClosing := False;
  FSocket.Connect(DISCORD_GATEWAY_URL);
end;

procedure TDicordTalkBot.Send(
  const iChannelID, iMessage: String;
  const iToUser: String = '');
begin
  SendRaw(
    Format(CMD_CREATE_MESSAGE, [iChannelID]),
    CreateMessageBody(iMessage, iToUser),
    nil,
    '');
end;

procedure TDicordTalkBot.SendFile(
  const iChannelID, iMessage: String;
  const iFile: TStream;
  const iFileName: String;
  const iToUser: String = '');
begin
  SendRaw(
    Format(CMD_CREATE_MESSAGE, [iChannelID]),
    CreateMessageBody(iMessage, iToUser),
    iFile,
    iFileName);
end;

procedure TDicordTalkBot.SendHeartbeat;
begin
  if not FConnecting then
    Exit;

  if FSeq > -1 then
    SendPayload<Integer>(TOpCode.Heartbeat, FSeq)
  else
    SendPayload<TObject>(TOpCode.Heartbeat, nil);
end;

procedure TDicordTalkBot.SendIdentify;
var
  D: TIdentify;
begin
  FResuming := False;

  D.token := FToken;
  D.properties.os := GetOSName;
  D.properties.browser := 'pk_net_discord_bot';
  D.properties.device := 'delphi';
  D.compress := False;
  D.large_threshold := 250;
  SetLength(D.shard, 2);
  D.shard[0] := 0;
  D.shard[1] := 1;
  D.presence.status := 'online';
  D.presence.afk := False;

  SendPayload<TIdentify>(TOpCode.Identify, D);
end;

function TDicordTalkBot.SendPayload<T>(
  const iOpCode: TOpCode;
  const iD: T): String;
const
  PAYLOAD_JSON = '{"op":%d,"d":%s}';
begin
  var S := TJsonSerializer.Create;
  try
    // 10.3 Rio's Bug Payload 構造体を作って Serialize すると Linux でエラー
    var Json := Format(PAYLOAD_JSON, [Ord(iOpCode), S.Serialize(iD)]);
    FSocket.Send(Json);
  finally
    S.DisposeOf;
  end;
end;

procedure TDicordTalkBot.SendRaw(
  const iCMDURL, iPayloadJsonStr: String;
  const iStream: TStream;
  const iStreamName: String);
begin
  FHttpClient.UserAgent := Format(HEADER_USER_AGENT_VALUE, [SELF_VERSION]);

  var Header: TNetHeaders;
  SetLength(Header, 1);
  Header[0].Name := HEADER_AUTHORIZATION;
  Header[0].Value := Format(HEADER_AUTHORIZATION_VALUE, [FToken]);

  var FormData := TMultipartFormData.Create;
  try
    FormData.AddField('payload_json', iPayloadJsonStr);

    if iStream <> nil then
    begin
      FormData.AddStream(
        'file',
        iStream,
        iStreamName,
        ContentTypeToString(TRESTContentType.ctAPPLICATION_OCTET_STREAM));
    end;

    var Path := iCMDURL.Trim;
    if Path.StartsWith('/') then
      Path := Path.Substring(1);

    var Res := FHttpClient.Post(DISCORD_HTTP_URL + Path, FormData, nil, Header);

    if Res.StatusCode > 399 then
      CallOnError(ERR_CODE_SEND_FAILED, Res.ContentAsString(TEncoding.UTF8));
  finally
    FormData.DisposeOf;
  end;
end;

procedure TDicordTalkBot.SendResume;
var
  D: TResume;
begin
  D.token := FToken;
  D.session_id := FSessionID;
  D.seq := FSeq;

  SendPayload<TResume>(TOpCode.Resume, D);
end;

procedure TDicordTalkBot.SocketClosed(
  Sender: TObject;
  const iCloseCode: Integer;
  const iCloseData: TBytes);
begin
  FConnecting := False;

  if (not FClosing) and (FSeq > -1) then
  begin
    //FResuming := True;
    Open;
  end;
end;

procedure TDicordTalkBot.SocketError(
  Sender: TObject;
  const iErrCode: Integer;
  const iMessage: String);
begin
  if
    iErrCode
    in [
      TWebSocket.ERR_CODE_CONNECTION,
      TWebSocket.ERR_CODE_HANDSHAKE_TIMEOUT
    ]
  then
  begin
    Open;
  end;

  CallOnError(iErrCode, iMessage);
end;

procedure TDicordTalkBot.SocketHeartbeat(Sender: TObject);
begin
  SendHeartbeat;
end;

procedure TDicordTalkBot.SocketText(Sender: TObject; const iText: String);
var
  Json: TJsonValue;
  Code: Integer;
  EventType: String;
begin
  Json := TJsonObject.ParseJSONValue(iText) as TJsonObject;
  try
    var op := Json.FindValue('op');
    var d := Json.FindValue('d');

    Code := -1;
    if op <> nil then
      Code := StrToIntDef(op.Value, -1);

    case TOpCode(Code) of
      TOpCode.None:
        Exit;

      TOpCode.Dispatch:
      begin
        var s := Json.FindValue('s');
        var t := Json.FindValue('t');

        if s <> nil then
          FSeq := StrToIntDef(s.Value, -1);

        if d <> nil then
        begin
          EventType := '';
          if t <> nil then
            EventType := T.Value;

          if Assigned(FOnDispatch) then
            FOnDispatch(Self, EventType, d);

          if EventType = EVENT_READY then
          begin
            var Session := d.FindValue('session_id');
            if Session <> nil then
              FSessionID := Session.Value;

            var UserName := d.FindValue('user.username');
            if UserName <> nil then
              FUserName := UserName.Value;

            var UserId := d.FindValue('user.id');
            if UserId <> nil then
            begin
              FUserId := UserId.Value;
              FMentionTag := Format('<@%s>', [FUserId]);
              FMentionTagLen := FMentionTag.Length;
            end;

            if Assigned(FOnReady) then
              FOnReady(Self);
          end;

          if EventType = EVENT_RESUMED then
          begin
            FResuming := False;
          end;

          if EventType = EVENT_MESSAGE_CREATE then
          begin
            if Assigned(FOnMessage) then
              FOnMessage(Self, d.ToString);

            var Mentions := d.FindValue('mentions').AsType<TJsonArray>;
            if Mentions <> nil then
            begin
              for var M in Mentions do
              begin
                var MentionUserId := '';

                if
                  (M.TryGetValue('id', MentionUserId)) and
                  (not MentionUserId.IsEmpty) and
                  (MentionUserId = FUserId)
                then
                begin
                  var FromUserID := '';
                  var Msg := '';
                  var UserName := '';
                  var Disc := '';
                  var ChannelID := '';

                  var Content := d.FindValue('content');
                  if Content <> nil then
                  begin
                    var Body := Content.Value;
                    var Index := Body.IndexOf(FMentionTag);

                    if Index > -1 then
                      Msg := Body.Substring(Index + FMentionTagLen).TrimLeft;
                  end;

                  d.TryGetValue('channel_id', ChannelID);
                  var Author := d.FindValue('author');

                  if Author <> nil then
                  begin
                    Author.TryGetValue<String>('username', UserName);
                    Author.TryGetValue<String>('discriminator', Disc);
                    Author.TryGetValue<String>('id', FromUserID);
                  end;

                  if Assigned(FOnMention) then
                    FOnMention(
                      Self,
                      ChannelID,
                      FromUserID,
                      UserName + '#' + Disc,
                      Msg);

                  Break;
                end;
              end;
            end;
          end;
        end;
      end;

      TOpCode.InvalidSession:
      begin
        if FResuming then
          SendIdentify;
      end;

      TOpCode.Hello:
      begin
        FConnecting := True;

        if d <> nil then
        begin
          var Interval := d.FindValue('heartbeat_interval');
          if Interval <> nil then
            FSocket.HeartBeatInterval :=
              StrToIntDef(Interval.Value, -1) * 2 div 3;
        end;

        SendHeartbeat;
        FSocket.ResetHeartbeat;

        if FResuming then
          SendResume
        else
          SendIdentify;
      end;

      TOpCode.HeartbeatACK:
      begin
      end;
    end;
  finally
    Json.DisposeOf;
  end;
end;

{ TBotApplication }

procedure TBotApplication.BotDispatch(
  Sender: TObject;
  const iEventType: String;
  const iData: TJsonValue);
begin
  DoDispatch(iEventType, iData);
end;

procedure TBotApplication.BotError(
  Sender: TObject;
  const iErrCode: Integer;
  const iMessage: String);
begin
  DoError(iErrCode, iMessage);
end;

procedure TBotApplication.BotMention(
  Sender: TObject;
  const iChannelID, iFromUserID, iFromUserName, iMessage: String);
begin
  DoMention(iChannelID, iFromUserID, iFromUserName, iMessage);
end;

procedure TBotApplication.BotMessage(Sender: TObject; const iData: String);
begin
  DoMessage(iData);
end;

procedure TBotApplication.BotReady(Sender: TObject);
begin
  DoReady;
end;

constructor TBotApplication.Create(const iToken: String);
begin
  inherited Create;

  FCommandChar := DEF_COMMAND_CHAR;

  FBot := TDicordTalkBot.Create(iToken);
  FBot.OnError := BotError;
  FBot.OnReady := BotReady;
  FBot.OnDispatch := BotDispatch;
  FBot.OnMessage := BotMessage;
  FBot.OnMention := BotMention;
end;

destructor TBotApplication.Destroy;
begin
  Stop;
  FBot.DisposeOf;

  inherited;
end;

procedure TBotApplication.DoDispatch(
  const iEventType: String;
  const iData: TJsonValue);
begin
  if Assigned(FDispatchHandler) then
    FDispatchHandler(iEventType, iData);
end;

procedure TBotApplication.DoError(
  const iErrCode: Integer;
  const iMessage: String);
begin
  if Assigned(FErrorHandler) then
    FErrorHandler(iErrCode, iMessage);
end;

procedure TBotApplication.DoMention(
  const iChannelID, iFromUserID, iFromUserName, iMessage: String);
begin
  var Done := False;
  var CMD := iMessage.Trim;

  if CMD.StartsWith(FCommandChar) then
  begin
    var Index := CMD.IndexOf(' ');
    var Param := CMD.Substring(Index + 1);
    CMD := CMD.Substring(1, Index);

    if Assigned(FCommandHandler) then
      FCommandHandler(iChannelID, iFromUserID, iFromUserName, CMD, Param, Done);
  end;

  if (not Done) and Assigned(FMentionHandler) then
    FMentionHandler(iChannelID, iFromUserID, iFromUserName, iMessage);
end;

procedure TBotApplication.DoMessage(const iData: String);
begin
  if Assigned(FMessageHandler) then
    FMessageHandler(iData);
end;

procedure TBotApplication.DoReady;
begin
  if Assigned(FReadyHandler) then
    FReadyHandler;
end;

function TBotApplication.GetElapsedSeconds: UInt64;
begin
  Result := SecondsBetween(Now, FStartTime);
end;

function TBotApplication.GetOSName: String;
begin
  Result := FBot.GetOSName;
end;

procedure TBotApplication.Run;
begin
  FRunning := True;

  FStartTime := Now;

  TThread.CreateAnonymousThread(
    procedure
    begin
      try
        FBot.Open;

        // 接続を待つ
        var Start := Now;
        while not FBot.Connecting do
        begin
          TThread.Sleep(100);
          if MilliSecondsBetween(Now, Start) > CONNECTING_TIMEOUT_INTERVAL then
          begin
            FRunning := False;
            Break;
          end;
        end;

        // 接続中
        while FRunning do
          TThread.Sleep(100);
      finally
        FRunning := False;
      end;
    end
  ).Start;

  while FRunning do
    TThread.Sleep(100);
end;

procedure TBotApplication.Send(
  const iChannelID, iMessage: String;
  const iToUser: String = '';
  const iFile: TStream = nil;
  const iFileName: String = '');
begin
  if iFile = nil then
    FBot.Send(iChannelID, iMessage, iToUser)
  else
    FBot.SendFile(iChannelID, iMessage, iFile, iFileName, iToUser);
end;

procedure TBotApplication.Stop;
begin
  if FRunning then
  begin
    FBot.Close;
    FRunning := False;
  end;
end;

end.

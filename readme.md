# Discord Bot For Delphi 

# PLATFORMS
Windows / macOS / Linux

# ENVIRONMENT
Delphi 10.3.x Rio and up.  

# REQUIRED LIBRARY

PK.Net.WebSocket.pas  
  
**Repository**  
https://bitbucket.org/freeonterminate/websocket/

# LICENSE
  Copyright (C) 2019 HOSOKAWA Jun (Twitter: @pik)  
  Released under the MIT license  
  http://opensource.org/licenses/mit-license.php  

# HISTORY
2019/05/08 Version 1.0.0  

# USAGE

```delphi
program HelloBot;

{$APPTYPE CONSOLE}

uses
  PK.Net.Discord.Bot;

var
  Bot: TBotApplication;
begin
  Bot := TBotApplication.Create(TOKEN); // Bot Token
  try
    Bot.ErrorHandler :=
      procedure (const iErrCode: Integer; const iMessage: String)
      begin
        Writeln(Format('ERROR (%d): %s', [iErrCode, iMessage]));
      end;

    Bot.ReadyHandler :=
      procedure
      begin
        Writeln('Connected.');
      end;

    Bot.MentionHandler :=
      procedure (
        const iChannelID, iFromUserID, iFromUserName, iMessage: String)
      begin
        Writeln(
          Format(
            '#%s: %s(%s) say "%s"', 
            [iChannelID, iFromUserName, iFromUserID, iMessage])
        );
        Bot.Send(iChannelID, 'Hello, Discord!');
      end;

    Bot.Run;
  finally
    Bot.DisposeOf;
  end;
end.
```

